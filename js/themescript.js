jQuery(document).ready(function ($) {
	"use strict";
    /*------------------------------- slider ---------------------------------------*/
    $(function(){
        $('#mainslider').slick({
            dots: false,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            pauseOnFocus: false,
            pauseOnHover: false,
        });
        $('#offers-slider').slick({
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            pauseOnFocus: false,
            pauseOnHover: false,
        });

    });
    /*------------------------------- scroll to fix ---------------------------------------*/
    $(function () {
        /*$('.header-inner').scrollToFixed();*/
        var fix = $(".fixscroll");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= window.innerHeight) {
                fix.addClass("fixed");
            } else {
                fix.removeClass("fixed");
            }
        });


    });

    /*------------------------------- datepick ---------------------------------------*/
    $(function () {
        $('#date').datepicker({
             todayHighlight: true,
        });
    });
    /*------------------------------- timepick ---------------------------------------*/
    $(function () {
        $('#timepicker').timepicker({
            defaultTime: "09:30 AM",
        });
    });
    /*------------------------------- wow ---------------------------------------*/
    $(function() {
        new WOW().init();
    });
    /*------------------------------- Lightbox ---------------------------------------*/
    $(function(){
        $("a[data-rel^='prettyPhoto']").prettyPhoto({
            theme:'light_square',
            animationSpeed:'fast',
            slideshow:5000,
            show_title:true,
            overlay_gallery: false,
            social_tools:false

        });
    });
    /*------------------------------- loader ---------------------------------------*/
    $(function(){
        setTimeout(function(){
            $('body').addClass('loaded');
        }, 3000);
    });
    /*------------------------------- open mini cart ---------------------------------------*/
    $(function(){
        $("#open-mini-cart").on('click', function() {
            $('.cart-box').slideToggle('slow', function() {
            });
        });
    });
});
